<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;

if (CModule::IncludeModule("iblock")) {

    $IBLOCK_ID = 13;

    $arOrder = ["SORT" => "AES"];
    $arSelect = ["ID", "NAME", "IBLOCK_ID", "SECTION_PAGE_URL"];
    $arFilter = ["IBLOCK_ID" => $IBLOCK_ID, "ACTIVE" => "Y"];
    $res = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();

        $aMenuLinksExt[] = [
            $arFields['NAME'],
            $arFields['SECTION_PAGE_URL'],
            [],
            ['SCROLL_TO_ANCHOR' => 'Y', 'SECTION_ID' => $arFields['ID']],
            "",
        ];
    }

    $aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
}
?>